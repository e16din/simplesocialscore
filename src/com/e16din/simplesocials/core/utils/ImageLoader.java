package com.e16din.simplesocials.core.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class ImageLoader extends AsyncTask<Void, Integer, Void> {
	
	private String url = null;
	private byte[] data = null;
	private Bitmap bitmap = null;
	private OnImageLoaderListener listener = null;

	public ImageLoader(String url, OnImageLoaderListener listener) {
		this.url = url;
		this.listener = listener;
	}

	@Override
	protected Void doInBackground(Void... params) {
		bitmap = getBitmapFromUrl(url);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 95, baos);
		data = baos.toByteArray();
		return null;
	}
	
	public static Bitmap getBitmapFromUrl(String link) {
		Log.d("qqqa", "link: "+link);
		try {
			URL url = new URL(link);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(Void result) {
		this.listener.onCompleted(data, bitmap);
		super.onPostExecute(result);
	}
	
	public interface OnImageLoaderListener {
		public void onCompleted(byte[] data, Bitmap bitmap);
	}
}