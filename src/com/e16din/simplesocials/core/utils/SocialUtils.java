package com.e16din.simplesocials.core.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

public class SocialUtils {
	public static void printFacebookKeyHash(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.i("Facebook KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static void printVkontakteFingerprints(Context context) {
		String[] fingerprints = null;
		try {
			if (context != null && context.getPackageManager() != null) {
				PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),
						PackageManager.GET_SIGNATURES);
				assert info.signatures != null;
				String[] result = new String[info.signatures.length];
				int i = 0;
				for (Signature signature : info.signatures) {
					MessageDigest md = MessageDigest.getInstance("SHA");
					md.update(signature.toByteArray());
					result[i++] = toHex(md.digest());
				}
				fingerprints = result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < fingerprints.length; i++)
			Log.i("Vkontakte Fingerprint:", fingerprints[i]);
	}

	private static String toHex(byte[] bytes) {
		BigInteger bi = new BigInteger(1, bytes);
		return String.format("%0" + (bytes.length << 1) + "X", bi);
	}
}
