package com.e16din.simplesocials.core;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.e16din.simplesocials.core.interfaces.BaseSocialManger;

public abstract class BaseSocialActionBarActivity extends ActionBarActivity {

	//private static BaseSocialManger currentManager = null;

	public abstract BaseSocialManger initSocialNetworks();// must return current manager

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BaseSocialManger.setCurrentManager(initSocialNetworks());
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// super.onActivityResult(requestCode, resultCode, data);
		BaseSocialManger.getCurrentManager().onActivityResult(this, requestCode, resultCode, data);

		// throw new NullPointerException("3333");
		//Log.i("qqq", "onActivityResult3333");
	}

	@Override
	public void onResume() {
		super.onResume();
		BaseSocialManger.getCurrentManager().onResume(this);

		//Log.i("qqq", "onResume4444");
		// throw new NullPointerException("4444");
	}

	@Override
	public void onPause() {
		BaseSocialManger.getCurrentManager().onPause(this);
		super.onPause();
	}

	@Override
	public void onDestroy() {
		BaseSocialManger.getCurrentManager().onDestroy(this);
		super.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (BaseSocialManger.getCurrentManager() != null)
			BaseSocialManger.getCurrentManager().onSaveInstanceState(this, outState);
	}

//	public BaseSocialManger getCurrentManager() {
//		return currentManager;
//	}
//
//	public void setCurrentManager(BaseSocialManger currentManager) {
//		this.currentManager = currentManager;
//		this.currentManager.setOnNoInternetConnectionListener(new Runnable() {
//			@Override
//			public void run() {
//				Toast.makeText(BaseSocialActionBarActivity.this, "No Internet connection!", Toast.LENGTH_SHORT).show();
//			}
//		});
//	}
}
