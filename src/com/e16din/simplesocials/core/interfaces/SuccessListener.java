package com.e16din.simplesocials.core.interfaces;

public interface SuccessListener {
	public void onSuccess(BaseSocialManger managerBox, String supportStr);

	public void onFail(String error, BaseSocialManger managerBox);

	public void onCancel();
}
