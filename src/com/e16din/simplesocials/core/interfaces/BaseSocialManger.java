package com.e16din.simplesocials.core.interfaces;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.e16din.lightutils.Utils;

public abstract class BaseSocialManger {

	public static final String PREFERENCE_NAME = "twitter_oauth";
	public static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";

	protected static SharedPreferences sharedPreferences = null;
	private static BaseSocialManger currentManager = null;

	private String token = null;
	private Activity contextActivity = null;
	private Runnable onNoInternetConnectionListener = null;

	public BaseSocialManger(Activity activity) {
		this.setContextActivity(activity);
		sharedPreferences = ((Activity) activity).getApplicationContext().getSharedPreferences(PREFERENCE_NAME, 0);
		setToken(sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), ""));
	}

	public abstract String getName();

	public boolean isAuth() {
		this.token = sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), "");
		return !TextUtils.isEmpty(token);
	}

	public void login(SuccessListener listener) {
		setCurrentManager(this);
		// this.setCurrentListener(listener);

		onNoInternetConnection(listener);
	}

	protected boolean onNoInternetConnection(final SuccessListener listener) {
		return !Utils.runIfOnline(false, ((Activity) getContextActivity()), new Runnable() {
			@Override
			public void run() {
				onNoInternetConnectionListener.run();
				listener.onFail("no internet connection", BaseSocialManger.this);
				return;
			}
		});
	}

	public void logout(SuccessListener listener) {
		setCurrentManager(this);
		// this.setCurrentListener(listener);

		setToken("");
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.remove(PREF_KEY_OAUTH_TOKEN + getName());
		editor.commit();
	}

	protected abstract void postPhoto(String message, String postUrl, String mediaUrl, SuccessListener listener);

	protected abstract void postVideo(String message, String postUrl, String mediaUrl, SuccessListener listener);

	protected void loginBeforeSharing(final boolean isVideo, final String message, final String postUrl,
			final String mediaUrl, final SuccessListener listener) {

		Log.d("qqqa", "loginBeforeSharing+isVideo: "+isVideo);
		
		if (onNoInternetConnection(listener))
			return;

		login(new SuccessListener() {
			@Override
			public void onSuccess(BaseSocialManger manager, String supportStr) {
				if (isVideo)
					postVideo(message, postUrl, mediaUrl, listener);
				else
					postPhoto(message, postUrl, mediaUrl, listener);
			}

			@Override
			public void onFail(String error, BaseSocialManger manager) {
				listener.onFail("loginBeforeSharing fail", manager);
			}

			@Override
			public void onCancel() {
				listener.onCancel();
			}
		});
	}

	public abstract void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data);

	public void onResume(Activity activity) {
	}

	public void onPause(Activity activity) {
	}

	public void onStop(Activity activity) {
	}

	public void onDestroy(Activity activity) {
	}

	public void postToWall(String message, String postUrl, String mediaUrl, boolean isVideo, SuccessListener listener) {
		setCurrentManager(this);

		if (onNoInternetConnection(listener))
			return;

		message = message == null ? "" : message;
		if (isVideo)
			postVideo(message, postUrl, mediaUrl, listener);
		else
			postPhoto(message, postUrl, mediaUrl, listener);
	}

	public String getToken() {
		return token;
	}

	// XXX: set token when receive it
	public void setToken(String token) {
		this.token = token;

		Editor editor = sharedPreferences.edit();
		editor.putString(PREF_KEY_OAUTH_TOKEN + getName(), getToken());
		editor.commit();
	}

	public Activity getContextActivity() {
		return contextActivity;
	}

	public void setContextActivity(Activity contextActivity) {
		this.contextActivity = contextActivity;
	}

	public void onSaveInstanceState(Activity activity, Bundle outState) {
		// TODO:
	}

	public void setOnNoInternetConnectionListener(Runnable listener) {
		onNoInternetConnectionListener = listener;
	}

	public static BaseSocialManger getCurrentManager() {
		return currentManager;
	}

	public static void setCurrentManager(BaseSocialManger currentManager) {
		BaseSocialManger.currentManager = currentManager;
	}
}
